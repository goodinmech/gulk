var GulkCLI   = require('./cli.js');
var gulkBuild = require('./tasks/build');
var gulkSend  = require('./tasks/send');
var gulkWatch  = require('./tasks/watch');

var cliCommands = {
  build: function() {
    gulkBuild.run();
  },
  send: function() {
    gulkSend.run();
  },
  watch: function() {
    gulkWatch.run();
  },
  nowatch: function() {
    gulkWatch.stop();
  }
};

cli = new GulkCLI(cliCommands);
cli.start();
